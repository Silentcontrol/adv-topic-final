var acme = acme || {};

acme.AfterMarketSport = function(){

		
		// we'll use this function as our generic callback
		//function handleResponse(response){
			//alert(response);
		
		
		//getAllParts(handleResponse);

		//var PartId = 2;
		//getPartById(PartId, handleResponse);

			
		//var newPart = {"title":"Bob's Life Story", "author":"Bob"}; 
		//Note: no need add an 'id' the newPart object, the database will assign the id
		//and the newPart, including the id, will be in the response
		//postPart(newPart, handleResponse);


		//var updatedPart = {"id":1, "title":"The Part of Stuff", "author":"Bob"};
		//putPart(updatedPart, handleResponse);

		
		/*
		YOU JOB IS TO IMPLEMENT THE CODE FOR THE FOLLOWING METHODS 
		(refer to the api docs for the Part web service)
		Once you get all that working, you can start working on the PartDetails module (see acme/Part-details.js)
		This module will display an HTML form that will allow users to add new Parts, and edit existing Parts.
		*/
		var ajax = acme.ajax;

		if(!ajax){
			throw new Error("This module depnds on the acme.ajax module");
		}




		function getAllParts(callback){
			// TODO: use your acme.ajax module to send the proper request to the Part web service
			// to get a Part by it's id
			acme.ajax.send({
                callback:callback,
                url: "http://aftermarketsport.com/adv-topics-final/web-service/Parts",
                method: 'GET'
                
            });
           
		}
			// Note: you can comment out this line of code...
			//callback("TODO: GET Part BY ID: " + id);
		function getPartById(id, callback){
			acme.ajax.send({
                callback:callback,
                url: "http://aftermarketsport.com/adv-topics-final/web-service/Parts" + id,
                method: 'GET'
                
            });
		}

		function postPart(Part, callback){
			// TODO: use your acme.ajax module to send the proper request to the Part web service
			// in order to add a Part

			// Note: you can comment out this line...
			// callback("TODO: INSERT NEW Part: " + JSON.stringify(Part));
			acme.ajax.send({
				callback:callback,
				url: "http://aftermarketsport.com/adv-topics-final/web-service/Parts",
				method: 'POST',
				requestbody: JSON.stringify(Part)

			});
		}
	

		function putPart(Part, callback){
			// TODO: use your acme.ajax module to send the proper request to the Part web service
			// in order to add a Part
			// Note: you can comment out this line...
			// callback("TODO: UPDATE A Part: " + JSON.stringify(Part));
				acme.ajax.send({
				callback:callback,
				url: "http://aftermarketsport.com/adv-topics-final/web-service/Parts",
				method: 'POST',
				requestbody: JSON.stringify(Part)

			});
	
	}

	return{
		getAllParts:getAllParts,
		getPartById:getPartById,
		postPart:postPart,
		putPart:putPart
	}

};
