var acme = acme || {};

acme.ajax = {
    
   
	send: function(options){

        var http = new XMLHttpRequest();
        var callback = options.callback;
        var url = options.url;
        var method = options.method;
        var header = options.header;
        var requestbody = options.requestbody;
        
        // This is the path you'll want to use ------> http://localhost/adv-topics/book-web-service/books/
       http.open(method, url);

       if(options.header){
       		for(var key in options.header){
       			http.setRequestHeader(key, options.header[key]);
       		}

       }
		// send the XmlHttpRequest, and if the http.readyState == 4 and the http.status == 200
        http.onreadystatechange = function() {
		        if(http.readyState == 4 && http.status == 200) {
                   callback(http.responseText);
		        }else if(http.readyState == 4){
		        	
		        }
		    };
         
        http.send(requestbody);


		// if the http.readyState == 4 and the http.status is NOT 200 then invoke the error method (defined below)
		// and pass the status as a param, like this...
		//this.error(http.status);
        
        /*
        Modules must be consistant to this
        Callback
        method
        headers
        url
        data/body
        error callback
        */
	},
	error: function(errMsg){
		alert(errMsg);
	}
};