var acme = acme || {};

acme.PartDetails = {
	
	init: function(options){

		// Instance Vars
		var target = options.target;
		var Part = options.Part;
        var Callback = options.Callback;
		
        createUI();
		var txtManufacturer;
		var txtPart;
		var txtId;
		var btnSave;
		var btnEdit;
	
        if(!Part){
            Part = {id: "", Parts: "", Manufacturer: ""};
        }
        
		// Methods
		function createUI(){
            target.innerHTML = "";
			
			txtId = document.createElement('input');
			txtId.setAttribute("type", "text");

			txtPart = document.createElement('input');
			txtPart.setAttribute("type", "text");

			txtManufacturer = document.createElement('input');
			txtManufacturer.setAttribute("type", "text");

			btnSave = document.createElement('input');
			btnSave.setAttribute("type", "Button");
			
            btnSave.addEventListener('click', function(){
                /*
                if(Part.title != txtTitle.value){
					alert("Please enter valid data");
				}else{
				alert("Thank you for submiting");
				//Part.id = txtId.value;
				}
                */
				Part.Parts = txtPart.value;
				Part.Manufacturer = txtManufacturer.value;
                Part.id = txtId.value;
                Callback(Part);
            });
			
			target.appendChild(txtPart);
			target.appendChild(txtManufacturer);
			target.appendChild(btnSave);
            btnSave.value = "Save";
            
            
		}
		function setPart(newPart){
			Part = newPart;
			txtPart.value = Part.Parts;
			txtManufacturer.value = Part.Manufacturer;
			txtId.value  = Part.id;

		}

        function clear(){
            txtPart.value = "";
            txtId.value=0;
            txtManufacturer.value = "";
        }


		// Return the public API
		return{
			setPart:setPart,
            clear:clear
		};
	}

};