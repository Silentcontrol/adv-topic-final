var acme = acme || {};

acme.PartList = {
	
	init: function(options){

		// Instance Vars
		var target = options.target;
		var data = options.data;
        var selectedCallBack = options.selectedCallBack;
        createUI();
        
        function createUI(){
            target.innerHTML = "";
            
            var ul = document.createElement('ul');
           // target.appendChild(ul);
            // Methods
            for(var i = 0; i < data.length; i++){
                var item = document.createElement('li');
                item.appendChild(document.createTextNode(data[i]['Parts'] + ", "));
                item.appendChild(document.createTextNode(data[i]['Manufacturer'] +" "));
                var editButton = document.createElement('button');          
                var button = item.appendChild(document.createElement("input"));
                button.type = "button";
                button.value = "Edit";
                button.Part = data[i];
                button.addEventListener('click', function(e){
                 var PartId = e.target.getAttribute("data-Part-id");
                   var Part = e.target.Part;
                    if(selectedCallBack){
                        selectedCallBack(Part);
                    }
                });
                ul.appendChild(item);  
            }
            target.appendChild(ul);
        }
        
        
        function setData(Part){
            data = Part;
            refreshUI();
            clear();
        }
        
        function getPartByID(id){
            for(var i=0; i < data.length; i++){
                if(id == data[i].id){
                    return data[i];
                }
            }
        }
        
        function addPart(Part){
            data.push(Part);
            refreshUI();
        }
        
        function refreshUI(){
           target.innerHTML = "";
            createUI();
        }
    
		// Return the public API
		return{
            selectedCallBack:selectedCallBack,
            createUI:createUI,
            target:target,
            setData:setData,
            getPartById:getPartByID,
            addPart:addPart,
            refreshUI:refreshUI
		};
    }
	

};