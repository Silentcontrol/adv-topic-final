window.addEventListener("load", function(){

	// Set up all the modules here...
		var da = acme.AfterMarketSport();
        var partList;
        var PartDetails;
        var part;
        acmeAjaxSend();


        function acmeAjaxSend(){
            acme.ajax.send({
                callback:setUpPartsList,
                url:"http://aftermarketsport.com/adv-topics-final/web-service/Parts",
                method:"GET"
            });
        }
        
		var PartDetails = acme.PartDetails.init({
			target:document.getElementById("parts-details"),
				Callback:function(part){
					if(part.id == null || part.id == 0){
						da.postPart(part, function(responses){
                            acmeAjaxSend();
                            console.log(part);
					    });
					}else{
                        da.putPart(part, function(responses){
                            partList.refreshUI();
                            PartDetails.clear();
						});
                    }
                }  
         });
        
		function setUpPartsList(responses){
			var parts = JSON.parse(responses);
			var div = document.getElementById("parts-list");      
            partList = acme.PartList.init({
				target:div,
				data:parts,
                selectedCallBack:function(part){
                    PartDetails.setPart(part);
                    if(part.id == null || parts.id ==0){
                        PartDetails.setPart(part);
                    }
                }
			});
		}
});