<?php
//header("Access-Control-Allow-Origin: *");
//die("foobar");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


if($_SERVER['SERVER_NAME'] == "localhost"){
    define("DB_HOST", "localhost");
    define("DB_USER", "root");
    define("DB_PASSWORD", "");
    define("DB_NAME", "aftermarketsport_serivce");
}else{
    define("DB_HOST", "localhost");
    define("DB_USER", "aftermar_admin");
    define("DB_PASSWORD", "JaXYi@17");
    define("DB_NAME", "aftermar_aftermarketsport_service");
}
// Pre-requisites:
// 	1. Url Re-writing (mod rewrite)
//	2. AJAX - We should have completed the Abstracting AJAX project
//	3. Regular expressions


///////////////////////////////////
// Set up the data access object
///////////////////////////////////
$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
require("AfterMarketSport.inc.php");
$da = new AfterMarketSport($link);



///////////////////////////////////
// Handle the Request
///////////////////////////////////

// Gather all the information about the request
$url_path = isset($_GET['url_path']) ? $_GET['url_path'] : ""; // This is the path part of the URL being requested (see the .htaccess file)
$method = $_SERVER['REQUEST_METHOD'];
$request_body = file_get_contents('php://input'); // note that GET requests do not have request body
$request_headers = getallheaders();


// This IF statement will check to see if the requested URL (and method) is supported by this web service
// If so, then we need to formulate the proper response, if not then we return a 404 status code
if($method == "GET" && empty($url_path)){

 	//Show the home page for this web service
  require("api-docs.php");
  die();

}else if($method == "GET" && ($url_path == "Parts/" || $url_path == "Parts")){

  get_all_Parts();

}else if($method == "POST" && ($url_path == "Parts/" || $url_path == "Parts")){
  	
  insert_Part();
  
}else if($method == "GET" && preg_match('/^Parts\/([0-9]*\/?)$/', $url_path, $matches)){
   
  // Get the id of the Part from the regular expression
  $Part_id = $matches[1];
  get_Part_by_id($Part_id);

}else if($method == "PUT" && preg_match('/^Parts\/([0-9]*\/?)$/', $url_path, $matches)){

  // Get the id of the Part from the regular expression
  //$Part_id = $matches[1]; // we can get the Part id from the request body
  update_Part();  
  
}else{

  header('HTTP/1.1 404 Not Found', true, 404);
  die("We're sorry, we can't find this page: {$_SERVER['REQUEST_URI']}");

}


/////////////////////////////////
// FUNCTIONS
/////////////////////////////////

function get_all_Parts(){

  global $da, $url_path, $method, $request_body, $request_headers;
  
  // check to see if we need to sort the Parts by author or title
  $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : null;
  
  // get the Parts from the database
  $Parts = $da->get_all_Parts($order_by);
  if(!$Parts){
    header('HTTP/1.1 500 server error (fetching Parts from the database)', true, 500);
    die();
  }
    
  // We'll default to returning Parts in JSON format unless the client
  // requests XML (the Accept header in the request would be set to application/xml)
  $return_format = $request_headers['Accept'];
  
  if($return_format == "application/xml"){
    //header("Content-Type","application/xml");
    die("TODO: convert Part data to XML");
  }else{
    //header("Content-Type","application/json");
    $json = json_encode($Parts);
    echo($json);
    die();
  }

}


function insert_Part(){

  global $da, $url_path, $method, $request_body, $request_headers;

  // The data for the Part being inserted should be in the request body.
  // The data should be sent in the JSON format which means that the Content-Type header in the request SHOULD be set to application/json
  // But if it's not set properly, we'll just assume the data is coming in as JSON
  // In the future, we might also accept the data in the request body to be XML, which would mean that the Content-Type header is set to application/xml
  
  $new_Part = null;
  
  if($request_headers['Content-Type'] == "application/xml"){
    die("TODO: convert XML Part data into an associative array");    
  }else{
    // convert the json to an associative array
    // note: json_decode() will return false if it can't convert the request body (maybe because it's not valid json)
    $new_Part = json_decode($request_body, true);
  }

  // TODO: validate the $new_Part assoc array (make sure it has id, title and author keys)

  if($new_Part){
    
    if($new_Part = $da->insert_Part($new_Part)){
      // note that the Part returned by insert_Part() will have the id set (by the database auto increment)
      // TODO: if the Accept header in the request is set to application/xml, then the client want the data returned in XML, we could deal with that later  
      // For now, we'll just return the data in JSON format
     // header("Content-Type","application/json");
      die(json_encode($new_Part));
    }else{
      header('HTTP/1.1 500 server error (fetching Parts from the database)', true, 500);
      die();  
    }
  
  }else{
    header('HTTP/1.1 400 - the data sent in the request is not valid', true, 400);
    die();
  }

}


function get_Parts_by_id($id){

  global $da, $url_path, $method, $request_body, $request_headers;

  $Part = $da->get_Part_by_id($id);

  // TODO: we may want to check the Accept header in the request, and if it's set to application/xml, we might return the data as XML instead of JSON
  // But for now, we'll just return the Part data as JSON
  
  if($Part){
    //header("Content-Type","application/json");
    die(json_encode($Part));
  }else{
    header('HTTP/1.1 400 - the Part id in the requested url is not in the database', true, 400);
    die();
  }

}


function update_Part(){

  global $da, $url_path, $method, $request_body, $request_headers;

  if($Part = json_decode($request_body, true)){

    $Part = $da->update_Part($Part);

    if($Part){
      //header("Content-Type","application/json");
      die(json_encode($Part));
    }else{
      header('HTTP/1.1 500 - Unable to update Part in the database', true, 500);
      die();
    }

  }else{
    header('HTTP/1.1 400 - Invalid Part data in the request body', true, 400);
    die();
  }

}
?>