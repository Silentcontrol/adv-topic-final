<?php

class AfterMarketSport{
	
	private $link;

	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}


	/**
	* Get all parks
	* @param $order_by 		Optional - the sort order (title or author)
	* @return 2d array 		Returns an array of parks (each park is an assoc array)
	*/
	function get_all_Parts($order_by = null){
		// TODO: if the order_by param is not null we need to sort the parks (should be by title or author)
		$qStr = "SELECT	id, Parts, Manufacturer FROM AfterMarketSport";

		if($order_by == "Parts" || $order_by == "Manufacturer"){
			$qStr .= " ORDER BY " . $order_by;
		}
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or die (mysqli_error($this->link));
		$all_parts = array();

		while($row = mysqli_fetch_assoc($result)){

			$part = array();
			$part['id'] = htmlentities($row['id']);
			$part['Parts'] = htmlentities($row['Parts']);
			$part['Manufacturer'] = htmlentities($row['Manufacturer']);

			$all_parts[] = $part;
		}

		return $all_parts;
	}


	/**
	* Get a park by its ID
	* @param $id 			The ID of the park to get
	* @return array 		An assoc array that has keys for each property of the park
	*/
	function get_part_by_id($id){
		
		// TODO: if the order_by param is not null we need to sort the parks (should be by title or author)
		$qStr = "SELECT	id, Parts, Manufacturer FROM AfterMarketSport WHERE id = " . mysqli_real_escape_string($this->link, $id);

				
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$part = array();
			$part['id'] = htmlentities($row['id']);
			$part['Parts'] = htmlentities($row['Parts']);
			$part['Manufacturer'] = htmlentities($row['Manufacturer']);
			return $part;

		}else{
			return null;
		}
			
	}


	function insert_part($part){

		// prevent SQL injection
		$part['Parts'] = mysqli_real_escape_string($this->link, $part['Parts']);
		$part['Manufacturer'] = mysqli_real_escape_string($this->link, $part['Manufacturer']);
		

		$qStr = "INSERT INTO AfterMarketSport (
					Parts,
					Manufacturer
				) VALUES (
					'{$part['Parts']}',
					'{$part['Manufacturer']}'
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the park id that was assigned by the data base
			$part['id'] = mysqli_insert_id($this->link);
			// then return the park
			return $part;
		}else{
			$this->handle_error("unable to insert part");
		}

		return false;
	}



	function update_part($part){

		// prevent SQL injection
		$part['id'] = mysqli_real_escape_string($this->link, $part['id']);
		$part['Parts'] = mysqli_real_escape_string($this->link, $part['Parts']);
		$part['Manufacturer'] = mysqli_real_escape_string($this->link, $part['Manufacturer']);

		$qStr = "UPDATE AfterMarketSport SET Parts='{$part['Parts']}', Manufacturer='{$part['Manufacturer']}' WHERE id = " . $part['id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $part;
		}else{
			$this->handle_error("unable to update part");
		}

		return false;
	}

    
}