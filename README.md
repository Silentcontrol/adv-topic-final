# Make sure to update this README file so that it describes your project #

### What is the project about ###

This project...

### Getting Set Up ###

This project use NodeJS and Gulp. In order to get started you will have to do these things after cloning the reposiroty

* Open the terminal and cd to the project folder
* Run this command to install the required NodeJS modules(the dependencies are in package.json): npm install
* You will make all your changes (and test the code) in the 'src' folder

### Adding Modules ###

To add a module to the app:

* Create a folder for the module in src/js/modules/
* Create a .js file for the module in the folder
* Add a script tag to src/index.html that links to the module .js file
* Put the code that initializes the module in src/js/main.js
* Add the file to your gulpfile.js (the task is named processJS) so that it will be included in dist/js/main.min.js
* If the module has a .css file, add an import statement to both src/styles/main.css and src/styles/main.scss (note that by adding it to the .css file it will work in when running from the src folder, by adding it to the .scss file, it will be compiled into the main.min.css in the dist folder)
* If the module has a .scss file, you  only need to import it in the main.scss (but you'll have to compile the .scss file in order to see changes when you are working in the src folder)

### Building and Deploying the App ###

To compile the app for distribution:

* Open the terminal and cd to the project folder
* Enter this command to run the Gulp tasks: gulp
* The compiled code for distribution will be in the 'dist' folder
* Copy the dist folder to the production server
* Copy the web-service folder to the production server